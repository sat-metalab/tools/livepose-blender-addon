# LivePose real-time MoCap with Blender

The Blender add-on for [LivePose](https://gitlab.com/sat-metalab/livepose) is a real-time 3D animation tool allowing for simplified live and full body [motion capture](https://en.wikipedia.org/wiki/Motion_capture). The ongoing development works toward implementing the possibility to have full control over the face, body and fingers. It permits to pick rigged 3D models from your scene and edit the links between their armature bones and the LivePose's detected keypoints, while processing incoming LivePose data. Hence, it is suitable for real-time animation of a human in a virtual environment.

## Table of Contents

- [Prerequisites](#prerequisites)
- [How to install](#how-to-install)
- [How to use](#how-to-use)

## Prerequisites

- You must have a version of Blender 2.8+.
- You must have the archive _livePoseAddon.zip_ somewhere on your hard drive.
Download it [here](https://gitlab.com/sat-metalab/tools/livepose-blender-addon/-/jobs/artifacts/main/download?job=archive-generation-job).
- You must use [LivePose](https://gitlab.com/sat-metalab/livepose) to send the captured movements OSC data to the add-on.

## How to install

- Compress the _livePoseAddon_ folder in .zip format
- Open Blender and go to _Edit/Preferences_.
- Go into the _Add-ons_ tab and press the _Install..._ button.
- Go into the directory containing the archive _livePoseAddon.zip_.
- Select the file and press _Install Add-on from File..._.
- You should see a new line inside the Add-ons viewer with _Development: LivePose Capture_ as a title. You can read the description by pressing the little arrow and you must check the box to enable the add-on.

## How to use

- Inside the 3D Viewer, you can see a sidebar menu on the right. If not, you can go into the _View_ menu (on top of the viewer) and check _Sidebar_.
- Now, you can access to the _LivePose Capture_ tab from this _Sidebar_.

There are several pannels from which you can manage differents parameters.

- **Rig Configuration:** You can parent a selected armature to a chosen avatar from this panel, that you will then be able to control through LivePose's OSC messages. To do so:
  - Select the armature you want to control as an "Armature".
  - Select the mesh you want to use as an "Avatar".
  - Click on the _Apply rig_ button to automatically generate a Rigify rig and parent it to the selected avatar.

- **Input Mode:** In the future of its development, the add-on may provide the ability to use Libmapper as an input.

- **OSC Configuration:** This panel allows you to configure the OSC messages reception server. From it, you can:

- **Filter Configuration:** Select the indexes of the camera and pose from which you want to process the OSC data.

  - Set the IP address and port the LivePose OSC data capture server should listen on.
  - Start and stop a capture.

- **Calibration Configuration:** Calibrate the movements based on the resolution of your video capture and the dimensions of the avatar to control.
