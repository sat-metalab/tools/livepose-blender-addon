# Record animations using LivePose Add on for Blender

> [French version](#french-version) below.

### English version

---

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/meta-rig.png" />  
  <figcaption><b>- fig.1: Rigify's meta-rig creation -</b></figcaption>
  </figure>
</div>

- In `Object Mode`, create a Human armature ("meta-rig") using Rigify add-on (_fig.1_).  
  <br>

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/armature-mesh.png" />  
  <figcaption><b>- fig.2: meta-rig applied to a dummy mesh model -</b></figcaption>
  </figure>
</div>

- Adjust the armature bones positions in `Edit Mode` so that they fit the mesh shape you want the armature to be applied to (_fig.2_).  
  **Note: we have here removed the head and hands of the armature, as the model we want to control doesn't need this level of detail.**  
  **Furthermore, face and hands motion capture are not yet fully supported.**

<br>

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/fig1.png" />  
  <figcaption><b>- fig.3: rig configuration panel -</b></figcaption>
  </figure>
</div>

- In the LivePose Capture side panel, pick the created armature, as `Armature: metarig` (_fig.3_).
- Pick a 3D model as an avatar to control. Here, the `dummy_obj` mesh for instance.
- Click the `Apply rig` button to generate the associated rig.  
  <br>

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/fig2OSC.png"/>   
  <figcaption><b>- fig.4: OSC input configuration panel -</b></figcaption>
  </figure>
</div>

- Choose OSC as `Input Mode` (_fig.4_).  
  **Note: Libmapper is not yet supported.**
- Configure on which <u>address</u> and <u>port</u> you want to listen incoming OSC data messages.
- Use `Start capture` to begin retrieving incoming LivePose data.
- Use `Stop capture` anytime to stop retrieving LivePose data.  
  <br>

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/fig3.png"/>
  <img src="./media/images/how-to-record-animations/fig3bis.png"/>   
  <figcaption><b>- fig.5: activation of the record button -</b></figcaption>
  </figure>
</div>

- When you feel ready to start recording, just click on the record button (which should then be colored in blue).
- You can stop recording whenever by clicking again on the record button.  
  <br>

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/keyframes.png"/> 
  <figcaption><b>- fig.6: keyframes recorded in the timeline -</b></figcaption>
  </figure>
</div>

- In the timeline (_fig.6_), you can see the recorded keyframes appear.  
  <br>

<div align="center">
  <figure>
  <img src="./media/images/how-to-record-animations/export.png"/>
  <figcaption><b>- fig.7: export menu -</b></figcaption>
  </figure>
</div>

- You can finally export the recorded result into the file format of your choice.  
  <br>

### French version

---

- En `Mode Objet`, créez une armature humaine ("meta-rig") à l'aide de l'extension Rigify (_fig.1_).
- Ajustez les positions des os de l'armature dans le `Mode Édition` afin qu'elles correspondent à la forme de l'objet maillé auquel vous souhaitez appliquer l'armature (_fig.2_).  
  **Remarque : nous avons ici supprimé la tête et les mains de l'armature, car le modèle que nous voulons contrôler n'a pas besoin de ce niveau de détail.**  
  **De plus, la capture de mouvement du visage et des mains n'est pas encore entièrement prise en charge.**
- Dans le panneau latéral de LivePose Capture, choisissez l'armature créée, comme `Armature : metarig` (_fig.3_).
- Choisissez un modèle 3D comme avatar à contrôler. Ici, le maillage `dummy_obj` par exemple.
- Cliquez sur le bouton `Apply rig` pour générer le rig associé.
- Choisissez OSC comme `Input Mode` (_fig.4_).  
  **Remarque : Libmapper n'est pas encore pris en charge.**
- Configurez sur quelle <u>adresse</u> et <u>port</u> vous voulez écouter les messages de données OSC entrants.
- Utilisez `Start capture` pour commencer à récupérer les données LivePose entrantes.
- Utilisez `Stop capture` à tout moment pour arrêter de récupérer les données de LivePose.
- Lorsque vous vous sentez prêt à commencer l'enregistrement, cliquez simplement sur le bouton d'enrergistrement (qui devrait alors devenir bleu).
- Vous pouvez arrêter l'enregistrement à tout moment en cliquant à nouveau sur le bouton d'enregistrement.
- Dans la chronologie ("timeline") (_fig.6_), vous pouvez voir apparaître les images clés enregistrées.
- Vous pouvez enfin exporter le résultat enregistré dans le format de fichier de votre choix.
