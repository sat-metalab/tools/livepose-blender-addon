# Useful functions to retrieve keypoints values and define rig's bones properties

from dataclasses import dataclass
from mathutils import Vector
from typing import Dict


@dataclass
class Keypoint:
    x: float
    y: float
    z: float


def initialize_with(initial_dict) -> Dict[str, Vector]:
    """Initialize a keypoints dict with the initial pose bones coordinates in a rig

    :param initial_dict: dict of pose bones and their coordinates
    :type initial_dict: Dict[str, Vector]
    :rtype: Dict[str, Vector]
    :return: initial dict with keypoints names as keys
    """

    return {
        "RIGHTWRIST": initial_dict['hand_ik.R'],
        "LEFTWRIST": initial_dict['hand_ik.L'],
        "RIGHTSHOULDER": initial_dict['shoulder.R'],
        "LEFTSHOULDER": initial_dict['shoulder.L'],
        #"MIDHIP": initial_dict['torso'],
        "RIGHTHIP": initial_dict['thigh_parent.R'],
        "LEFTHIP": initial_dict['thigh_parent.L'],
        "RIGHTANKLE": initial_dict['foot_ik.R'],
        "LEFTANKLE": initial_dict['foot_ik.L'],
        "RIGHTKNEE": initial_dict['thigh_ik.R'],
        "LEFTKNEE": initial_dict['thigh_ik.L'],
        "RIGHTELBOW": initial_dict['upper_arm_ik.R'],
        "LEFTELBOW": initial_dict['upper_arm_ik.L']
    }


def set_limit_distance_constraints(rig_pose):
    """ Set limit distance constraints between pose bones into a pose rig

    :param rig_pose: pose rig on which to apply limit distance constraints
    :type rig_pose: bpy.types.Pose
    """

    bones_to_limit = {
        "hand_ik.R": 'upper_arm_tweak.R',
        "hand_ik.L": 'upper_arm_tweak.L',
        "foot_ik.R": 'thigh_tweak.R',
        "foot_ik.L": 'thigh_tweak.L'
    }

    for bone_name in bones_to_limit:
        # Set bone as active
        pb = rig_pose.bones[bone_name]

        # Apply limit distance constraint
        ld = pb.constraints.new(type='LIMIT_DISTANCE')

        # Set target
        target_name = bones_to_limit[bone_name]
        ld.target = rig_pose.bones[target_name].custom_shape


def set_limit_distance_constraint(rig_pose, bone_name):
    """ Set a limit distance constraint on a pose bone into a pose rig

    :param rig_pose: pose rig, owner of the bone
    :param bone_name: name of the bone on which to apply the limit distance constraint
    :type rig_pose: bpy.types.Pose
    :type bone_name: str
    """

    bones_to_limit = {
        "hand_ik.R": 'upper_arm_tweak.R',
        "hand_ik.L": 'upper_arm_tweak.L',
        "foot_ik.R": 'thigh_tweak.R',
        "foot_ik.L": 'thigh_tweak.L'
    }

    if bone_name in bones_to_limit and bone_name in rig_pose.bones:
        # Set bone as active
        pb = rig_pose.bones[bone_name]
        
        if "Limit Distance" not in pb.constraints.keys():
            # Apply limit distance constraint
            ld = pb.constraints.new(type='LIMIT_DISTANCE')
            
            # Set target
            target_name = bones_to_limit[bone_name]
            ld.target = rig_pose.bones[target_name].custom_shape

def clear_limit_distance_constraint(rig_pose, bone_name):
    """ Remove the limit distance constraint on a pose bone into a pose rig

    :param rig_pose: pose rig, owner of the bone
    :param bone_name: name of the bone on which to remove the limit distance constraint
    :type rig_pose: bpy.types.Pose
    :type bone_name: str
    """

    bones_to_limit = {
        "hand_ik.R": 'upper_arm_tweak.R',
        "hand_ik.L": 'upper_arm_tweak.L',
        "foot_ik.R": 'thigh_tweak.R',
        "foot_ik.L": 'thigh_tweak.L'
    }

    if bone_name in bones_to_limit and bone_name in rig_pose.bones:
        # Set bone as active
        pb = rig_pose.bones[bone_name]

        # Clear limit distance constraint
        ld = pb.constraints["Limit Distance"]
        pb.constraints.remove(ld)


def get_map_rig(kp_dict) -> Dict[str, Vector]:
    """Associate keypoints coordinates values to pose bones names

    :param kp_dict: dict of keypoints and their coordinates
    :type kp_dict: Dict[str, Vector]
    :rtype: Dict[str, Vector]
    :return: kp coordinates dict with pose bones names as keys
    """

    return {
        # body keypoints
        # "nose": kp_dict['NOSE'],
        # "neck": kp_dict['NECK'],
        #"chest": (kp_dict['LEFTSHOULDER'] + kp_dict['RIGHTSHOULDER'])/2,
        "hand_ik.R": kp_dict['RIGHTWRIST'],
        "hand_ik.L": kp_dict['LEFTWRIST'],
        "foot_ik.R": kp_dict['RIGHTANKLE'],
        #"hips": (kp_dict['LEFTHIP'] + kp_dict['RIGHTHIP'])/2,
        "torso": (kp_dict['LEFTHIP'] + kp_dict['RIGHTHIP'])/2,
        "foot_ik.L": kp_dict['LEFTANKLE']

        # "": kp_dict['RIGHTEYE'],
        # "": kp_dict['LEFTEYE'],
        # "": kp_dict['RIGHTEAR'],
        # "": kp_dict['LEFTEAR'],
        # "": kp_dict['LEFTBIGTOE'],
        # "": kp_dict['LEFTHEEL'],
        # "": kp_dict['RIGHTBIGTOE'],
        # "": kp_dict['RIGHTHEEL'],
    }
