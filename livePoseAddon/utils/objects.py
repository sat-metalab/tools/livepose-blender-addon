"""
Useful functions for objects and associated data retrieving.
"""
import bpy


def get_obj(obj_name):
    """Gets an object data by name.

    :param obj_name: name of the object in the scene
    :type obj_name: string
    :rtype: bpy_types.Object
    :return: object data
    """
    try:
        obj = bpy.data.objects[obj_name]
        return obj
    except KeyError:
        return None


def initial_position(armature_pose):
    """Return dict with pose bones position vectors in a given pose armature.

    :param armature_pose: armature's pose
    type armature_pose: bpy.types.Pose
    :rtype: dict {string: Vector}
    return: dictionary with armature's pose bones coordinates associated to their name
    """
    return {bone.name: bone.head for bone in armature_pose.bones}


def get_objects_from_collection(col_name):
    """Returns objects from collection.

    :param col_name: name of the collection
    :type col_name: string
    :rtype: list of bpy_types.Object
    :return: list of objects from collection, None of no collection found
    """
    if collection_exists(col_name):
        col = bpy.data.collections[col_name]
        return [ob for ob in col.all_objects]
    else:
        return None


def get_inv_pb_dict(armature_pose):
    """Return dict with pose bones inverse matrix in a given pose armature.

    :param armature_pose: armature's pose
    type armature_pose: bpy.types.Pose
    :rtype: dict {string: Matrix}
    return: dictionary with armature's pose bones inverse Matrix associated to their name
    """
    filtered_kp = ["root", "torso",
                   "hand_ik.R", "hand_ik.L",
                   "foot_ik.R", "foot_ik.L",
                   "chest", "hips",
                   "shoulder.R", "shoulder.L",
                   "upper_arm_ik.R", "upper_arm_ik.L",
                   "thigh_ik.R", "thigh_ik.L"]

    return {bone_name: armature_pose.bones[bone_name].matrix.inverted() for bone_name in filtered_kp}
