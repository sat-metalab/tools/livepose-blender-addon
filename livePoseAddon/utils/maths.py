""" 
Useful maths functions to process spaces matrix and bones rotation angles.
"""
import bpy
from mathutils import Matrix, Vector


def get_kp_world_location(x, y, z):
    """ Get a LivePose's keypoint coordinates transformed into the Blender's world space

    :param x, y, z: keypoint's coordinates
    :type x: float
    :type y: float
    :type z: float
    :rtype: Vector
    :return: coordinates into Blender's world space
    """
    # Get properties panel
    props = bpy.data.scenes["Scene"].CapturePropertyGroup

    # Get resolution dimensions
    res_len = props.resolution[0]
    res_width = props.resolution[1]

    # Get model dimensions
    model_width = props.model_dim[0]
    model_height = props.model_dim[1]

    # Process new pb location coordinates in Blender world space, from LivePose space coordinates.
    # - LivePose x needs to be centered.
    #   By default, resolution's length is 720.
    # - LivePose z = 0 unless depth is processed.
    # - LivePose y is subtracted to resolution's width.
    #   By default, resolution's width is 1280.
    # Blender's X, Y & Z axis, are respectively corresponding to x, z and -y coordinates

    coo = Vector((((x * model_width)/res_len) - (model_width/2),
                  0,
                  ((res_width - y) * model_height)/res_width
                  ))

    return coo


def get_pb_local_location(inv_pb, inv_world, world_coo):
    """Get the local coordinates of a pose bone given its coordinates into the Blender world space.

    :param inv_pb: pb's matrix inverse
    :param inv_world: world's matrix inverse
    :param world_coo: global world coordinates to process into local coordinates
    :type inv_pb: Matrix
    :type inv_world: Matrix
    :type world_coo: Vector
    :rtype: Vector
    :return: pose bone's local space coordinates
    """
    # Get the local space coordinates
    loc_coo = inv_pb @ inv_world @ world_coo
    return Vector((loc_coo[0], loc_coo[1], loc_coo[2]))


def process_angles(kp_dict, rig_name, pb, kp_target):
    """Rotate a pose bone according to its angle difference with its keypoints vector.
    """
    rig = bpy.data.objects[rig_name].pose
    pb = rig.bones[pb]

    if kp_target[0] in kp_dict and kp_target[1] in kp_dict:
        # Get keypoints vector
        kp_v = kp_dict[kp_target[1]] - kp_dict[kp_target[0]]

        # Get pb vector
        pb_v = pb.tail - pb.head

        # Compute the rotation difference
        rd = pb_v.rotation_difference(kp_v)

        # Compute rotation matrix
        m = (
            Matrix.Translation(pb.head) @
            rd.to_matrix().to_4x4() @
            Matrix.Translation(-pb.head)
        )

        # Rotate bone
        pb.matrix = m @ pb.matrix


def rotate_matrix(rig_name, pb, angle):
    rig = bpy.data.objects[rig_name].pose
    pb = rig.bones[pb]

    rotation = Vector((0, 0, angle))

    # Compute rotation matrix
    m = (
        Matrix.Translation(pb.head) @
        Quaternion(rotation).to_matrix().to_4x4() @
        Matrix.Translation(-pb.head)
    )

    pb.matrix = m @ pb.matrix
