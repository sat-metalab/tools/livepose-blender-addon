"""
Create libmapper inputs for each rig's pose bones and handle their received data.
"""
import bpy


class MapperInput:
    """
    Class which receipts bones inputs via libmapper.
    """

    def __init__(self, bone: bpy.types.PoseBone):
        super(MapperInput, self).__init__()

        # libmapper producer device
        self._dev = libmapper.device("livepose-addon")

    def update_bone_inputs(self) -> None:
        """
        Receipt the position data for a given bone
        """

        self._dev.add_signal(libmapper.DIR_IN, "Keypoint-Position", 1,
                             libmapper.FLT, self.keypoint_handler)

        while True:
            self._dev.poll(0)

    def keypoint_handler(keypoint, length, x, y, z):
        try:
            keypoint = re.sub('[^A-Z]', '', keypoint)
            print("Keypoint : {0:15} | X : {1:22} | Y : {2:22} | Z : {3:22}".format(
                keypoint, x, y, z))
        except:
            print('exception : ', keypoint)
