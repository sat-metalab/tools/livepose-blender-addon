import json

import bpy


def save_settings(self, context):

    text_settings = None
    for text in bpy.data.texts:
        if text.name == '.addroutes_settings':
            text_settings = text

    if text_settings is None:
        bpy.ops.text.new()
        text_settings = bpy.data.texts[-1]
        text_settings.name = '.addroutes_settings'

    props = (
        # OSC
        'livepose_osc_udp_in',
        'livepose_osc_port_in',
        'livepose_osc_udp_out',
        'livepose_osc_port_out',
        'livepose_osc_in_enable',
        'livepose_osc_out_enable',
        'livepose_osc_settings',
        'livepose_osc_debug',
    )

    dico_settings = {}
    for prop in props:
        dico_settings[prop] = eval("bpy.context.window_manager."+prop)

    text_settings.clear()
    text_settings.write(json.dumps(dico_settings))
    print("saving settings done")


cls = (
)


def register():
    for c in cls:
        bpy.utils.register_class(c)


def unregister():
    for c in reversed(cls):
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
