import os, sys, site

# ---------- Plugins Information ----------#
bl_info = {
    "name": "LivePose Capture",
    "author": "Eliès Jurquet",
    "description": "Motion capture manager for the LivePose software",
    "version": (1, 0, 0),
    "blender": (2, 80, 0),
    "location": "3D View > Tool",
    "warning": "Requires external packages",
    "wiki_url": "https://gitlab.com/sat-metalab/tools/livepose-blender-addon",
    "tracker_url": "https://gitlab.com/sat-metalab/tools/livepose-blender-addon/-/issues",
    "category": "Development"
}

def verify_user_sitepackages():
    usersitepackagespath = site.getusersitepackages()
    print("usersitepackagespath",usersitepackagespath)

    if os.path.exists(usersitepackagespath) and usersitepackagespath not in sys.path:
        sys.path.append(usersitepackagespath)

verify_user_sitepackages()

from .ui import ui_registration

def reload_modules():
    from . import imports
    imports.manage_imports(True)


if "bl_info" in locals():
    reload_modules()

def register():
    ui_registration.register()


def unregister():
    ui_registration.unregister()


if __name__ == "__main__":
    register()
