"""
Main add-on GUI module.
"""
# ---------- Imports ----------#
import os
import bpy
from bpy.types import Panel
from mathutils import Matrix
import re
from addon_utils import check

from dataclasses import dataclass
from typing import Dict
import threading
from mathutils import Vector

# Add-on modules import
# from livePoseAddon import load_save
from ..utils import objects, maths, install_dependencies, livePoseWrapper
from . import ui_dependencies

# Third-party modules import
try:
    from pythonosc import dispatcher, osc_server
except (ImportError, ModuleNotFoundError) as e:
    print("Could not import pythonosc: ",e)
import rigify

# --------------------------------------------#

bpy.types.Scene.Armature = bpy.props.StringProperty()
bpy.types.Scene.Avatar = bpy.props.StringProperty()
kp_local_dict = {}
kp_world_dict = {}
inv_world = Matrix()
inv_pb_dict = {}


# ---------- Panel Class ----------#

class DefaultPanel:
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "LivePose Capture"


class LivePosePanel(DefaultPanel, Panel):
    bl_label = 'LivePose Capture'
    bl_idname = 'LIVEPOSE_PT_main_panel'

    def draw(self, context):
        # prefs = bpy.context.preferences.addons['LivePose'].preferences
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        # ----- Rig Box -----
        box_1 = layout.box()
        col_1_1 = box_1.column()
        col_1_1.label(text="Rig Configuration:")
        scn = context.scene
        col_1_1.prop_search(scn, "Armature", bpy.data, "armatures")
        # Pick the mesh to parent to selected armature
        col_1_1.prop_search(scn, "Avatar", bpy.data, "meshes")
        col_1_1.operator('livepose.livepose_armature')

        # ----- Capture Box -----
        col_2 = layout.column()
        row = col_2.row()
        row.prop(context.scene.CapturePropertyGroup,
                 "inputMode")   # Input mode parameter

        inputMode = context.scene.CapturePropertyGroup.inputMode
        # ----- OSC Box -----
        if inputMode == "O":
            box_2 = layout.box()
            col_2_1 = box_2.column()

            #col.prop(context.window_manager, 'livepose_osc_settings')
            col_2_1.label(text="OSC Configuration:")
            row_2_1 = col_2_1.row(align=True)
            row_2_2 = col_2_1.row(align=True)

            row_2_1.prop(context.scene.CapturePropertyGroup, "address")
            row_2_1.prop(context.scene.CapturePropertyGroup, "port")

            row_2_2.operator('livepose.livepose_start')
            row_2_2.operator('livepose.livepose_stop')

            # ----- Filter Box -----
            box_2bis = layout.box()
            col_2bis_1 = box_2bis.column()

            col_2bis_1.label(text="Filter Configuration:")
            row_2bis_1 = col_2bis_1.row(align=True)
            row_2bis_2 = col_2bis_1.row(align=True)

            row_2bis_1.prop(context.scene.CapturePropertyGroup, "cam_index")
            row_2bis_2.prop(context.scene.CapturePropertyGroup, "pose_index")

        elif inputMode == "L":
            box_2 = layout.box()
            col_2_1 = box_2.column()

            col_2_1.label(text="Libmapper Configuration:")
            row_2_1 = col_2_1.row(align=True)
            row_2_1.label(icon="ERROR", text="Not yet supported")

        # ----- Calibration Box -----
        box_3 = layout.box()
        col_3_1 = box_3.column()

        col_3_1.label(text="Calibration Configuration:")
        row_3_1 = col_3_1.row(align=True)

        row_3_1.prop(context.scene.CapturePropertyGroup, "resolution")
        if objects.get_obj(context.scene.Avatar) is not None:
            row_3_2 = col_3_1.row(align=True)
            row_3_2.prop(context.scene.CapturePropertyGroup, "model_dim")

        # ----- Record Box -----
        box_4 = layout.box()
        col_4_1 = box_4.column()

        row_4_1 = col_4_1.row(align=True)
        row_4_1.prop(context.scene.CapturePropertyGroup,
                     "bool_record", icon="REC")

        # ----- Export Box -----
        box_5 = layout.box()
        col_5_1 = box_5.column()

        col_5_1.label(text="Export:")
        row_5_1 = col_5_1.row(align=True)
        row_5_2 = col_5_1.row(align=True)
        row_5_3 = col_5_1.row(align=True)

        # ExportBVH.bl_idname
        row_5_1.operator('export_anim.bvh', text="BVH Export...")
        # ExportFBX.bl_idname
        row_5_2.operator('lp.export_fbx', text="FBX Export...")
        # ExportGLTF.bl_idname
        row_5_3.operator('export_scene.gltf', text="GLTF Export...")

        layout.separator()


class WarningPanel(DefaultPanel, Panel):
    bl_label = 'LivePose Capture'
    bl_idname = "LIVEPOSE_PT_warning_panel"

    @classmethod
    def poll(cls, context):
        return not install_dependencies.dependencies_installed

    def draw(self, context):
        layout = self.layout

        lines = [f"Please install the missing dependencies for \"LivePose Capture\".",
                 f"1. Open the preferences (Edit > Preferences > Add-ons).",
                 f"2. Search for the \"LivePose Capture\" add-on.",
                 f"3. Open the details section of the add-on.",
                 f"4. Click on the \"{ui_dependencies.InstallDependenciesButton.bl_label}\" button.",
                 f"   This will download and install the missing Python packages, if Blender has the required",
                 f"   permissions.",
                 f"  ",
                 f"Important: the Rigify add-on is required for the rig generation feature to work properly.",
                 f"You need to activate it in the Blender add-on preferences panel as well."]

        for line in lines:
            layout.label(text=line)


# ---------- OSC Capture Properties ----------#

class CapturePropertyGroup(bpy.types.PropertyGroup):
    """
    LivePose capture retrieval properties 
    """
    # OSC Server Management
    address: bpy.props.StringProperty(name="Listen on ", default="0.0.0.0")
    port: bpy.props.IntProperty(name="Input Port", default=8000, min=0000)
    # Filter Management
    cam_index: bpy.props.IntProperty(
        name="Camera index", description="Index of the camera to use", default=0)
    pose_index: bpy.props.IntProperty(
        name="Pose index", description="Index of the pose to capture", default=0)
    # Input Mode Management
    inputMode: bpy.props.EnumProperty(name='Input Mode', description='Choose desired data input mode',
                                      items={
                                          ('O', 'OSC',
                                           'Use OSC data messages as data flux'),
                                          ('L', 'Libmapper', 'Use libmapper')
                                      }, default='O')
    # Calibration Management
    resolution: bpy.props.IntVectorProperty(name="Camera resolution", description="Record dimensions",
                                            default=(640, 480), size=2
                                            )
    model_dim: bpy.props.FloatVectorProperty(name="Avatar dimensions", description="Avatar mesh dimensions",
                                             default=(0, 0),
                                             size=2
                                             )
    bool_record: bpy.props.BoolProperty(name="Record",
                                        description="Record animation keyframes",
                                        default=False)


# ---------- LivePose Settings ----------#

class LivePoseResetCalibration(bpy.types.Operator):
    """
    Reset stereo camera calibration
    """
    bl_idname = "livepose.reset_calibration"
    bl_label = "Reset stereo camera calibration"

    def execute(self, context) -> set:
        #bpy.context.preferences.addons[addonName].preferences.calibration_set = False
        return {'FINISHED'}


class LivePoseSaveUserPrefs(bpy.types.Operator):
    """
    Shortcut operator for saving user preferences
    """
    bl_idname = "livepose.save_userpref"
    bl_label = "Save calibration along user preferences"

    def execute(self, context) -> set:
        bpy.ops.wm.save_userpref()
        return {'FINISHED'}

# ---------- Start OSC data capture ----------#


def depth_handler(kp, x, y, z):
    global inv_pb_dict, inv_world

    # Get rig's name
    rig_name = "rig"
    rig = bpy.data.objects["rig"]

    inv_pb = inv_pb_dict["root"]

    if z != 0.0:
        loc_coo = inv_pb @ inv_world @ Vector((x, z, 0))
        rig.pose.bones["root"].location = loc_coo
        print("X : {0:22} | Y : {1:22} | Z : {2:22}".format(x, y, z))
    # for bone in rig.pose.bones:
        #bone.location[2] = z
     #   print("Keypoint : {0:15} | X : {1:22} | Y : {2:22} | Z : {3:22}".format(kp, x, y, z))


def keypoints_handler(keypoint, x, y, z):
    global kp_local_dict, inv_pb_dict, inv_world
    kp = re.sub('[^A-Z]', '', keypoint)
    # print("Keypoint : {0:15} | X : {1:22} | Y : {2:22} | Z : {3:22}".format(
    #        keypoint, x, y, z))

    # Get keypoint's world coordinates
    kp_world_dict[kp] = maths.get_kp_world_location(x, y, z)

    filtered_kp = {
        'RIGHTWRIST': "hand_ik.R",
        'LEFTWRIST': "hand_ik.L",
        'RIGHTANKLE': "foot_ik.R",
        'LEFTANKLE': "foot_ik.L",
        'RIGHTHIP': "torso",  # "hips",
        'LEFTHIP': "torso",  # "hips",
        'MIDHIP': "torso",
        # 'RIGHTSHOULDER': "chest",
        # 'LEFTSHOULDER': "chest",
        # 'NECK': "neck",
        # 'NOSE': "nose"
    }

    articulations = {
        # "neck": ("NOSE", "NECK"),
        "upper_arm_ik.L": ("LEFTSHOULDER", "LEFTELBOW"),
        "upper_arm_ik.R": ("RIGHTSHOULDER", "RIGHTELBOW"),
        "thigh_ik.L": ("LEFTHIP", "LEFTKNEE"),
        "thigh_ik.R": ("RIGHTHIP", "RIGHTKNEE")
    }

    if kp in filtered_kp:
        # Get associated pose bone's name
        pb_name = filtered_kp[kp]
        # Get rig's name
        rig_name = "rig"
        rig = bpy.data.objects["rig"]

        if pb_name in inv_pb_dict.keys():
            # Get the pose bone's inverted matrix
            inv_pb = inv_pb_dict[pb_name]
            # print("WORLD : ", kp_world_dict[kp])

            # if kp == 'RIGHTWRIST' or kp == 'LEFTWRIST':
            #    kp_world_dict[kp][1] = -0.5     # raise the conductor's arms forward
            # elif kp == 'RIGHTANKLE' or kp == 'LEFTANKLE':
            #    kp_world_dict[kp][2] = 0        # fix the feet on the ground

            # Compute the coordinates into the pb local space coordinates
            kp_local_dict[kp] = maths.get_pb_local_location(
                inv_pb, inv_world, kp_world_dict[kp])
            # print("LOCAL : ", kp_local_dict[kp])

            # Update pose
            rig_dict = livePoseWrapper.get_map_rig(kp_local_dict)

            # Process orientation

            # Set limit distance for bone
            livePoseWrapper.set_limit_distance_constraint(rig.pose, pb_name)

            # Set new location
            rig.pose.bones[pb_name].location = rig_dict[pb_name]

            #rig.pose.bones["torso"].location = rig_dict["hips"]

            # Clear limit distance for bone
            # livePoseWrapper.clear_limit_distance_constraint(rig.pose, pb_name)

            # Process articulations
            # for art in articulations:
            #    maths.process_angles(kp_world_dict, rig_name,
            #                         art, articulations[art])

            # Register position into current frame (frame = bpy.data.scenes['Scene'].frame_current)
            if bpy.context.scene.CapturePropertyGroup.bool_record:
                rig.pose.bones[pb_name].keyframe_insert("location")
                rig.pose.bones[pb_name].keyframe_insert("rotation_euler")


class CaptureServerThread(threading.Thread):

    def __init__(self, address, port):
        super(CaptureServerThread, self).__init__()

        # Get properties panel
        props = bpy.data.scenes["Scene"].CapturePropertyGroup

        # Dispatcher
        self.disp = dispatcher.Dispatcher()
        self.disp.map("/livepose/skeletons/{}/{}/keypoints/*".format(props.cam_index, props.pose_index),
                      keypoints_handler)

        self.address = address
        self.port = port
        self._stop_event = threading.Event()
        self.server = osc_server.ThreadingOSCUDPServer(
            (self.address, self.port), self.disp)

    def run(self):
        print("Serving on {}".format(self.address))
        self.server.serve_forever()

    def stop(self):
        # Clean-up server (close socket, etc.)
        self.server.server_close()
        # Tell the serve_forever() loop to stop and wait until it does
        self.server.shutdown()
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()


class DepthServerThread(threading.Thread):

    def __init__(self, address, port):
        super(DepthServerThread, self).__init__()

        # Dispatcher
        self.disp = dispatcher.Dispatcher()
        self.disp.map("/livepose/position/0/0",
                      depth_handler)

        self.address = address
        self.port = port
        self._stop_event = threading.Event()
        self.server = osc_server.ThreadingOSCUDPServer(
            (self.address, self.port), self.disp)

    def run(self):
        print("Serving on {}".format(self.address))
        self.server.serve_forever()

    def stop(self):
        # Clean-up server (close socket, etc.)
        self.server.server_close()
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()


class StartCaptureOperator(bpy.types.Operator):
    bl_idname = "livepose.livepose_start"
    bl_label = "Start capture"
    bl_description = "Retrieve LivePose data in real time"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        global kp_local_dict

        # Get selected armature object
        selected_armature = context.scene.Armature
        armature = context.object

        if armature is not None and armature.type == 'ARMATURE':
            # Store armature initial bones position in a dict
            initial_dict = objects.initial_position(armature.pose)

            # Initialize LivePose keypoints dict with armature dict
            kp_local_dict = livePoseWrapper.initialize_with(initial_dict)

        else:
            print("Not an armature")

        ########

        osc_udp_in = context.scene.CapturePropertyGroup.address
        osc_port_in = context.scene.CapturePropertyGroup.port

        try:
            captureServer = CaptureServerThread(osc_udp_in, osc_port_in)
            print("CAPTURING OSC DATA")
        except OSError as e:
            self.report({'ERROR'}, e.strerror)
            return {'CANCELLED'}

        captureServer.setDaemon(True)

        try:
            captureServer.start()
        except RuntimeError():
            self.report({'ERROR'}, "something went wrong !")
            return {'CANCELLED'}
        else:
            self.report({'INFO'}, "all done!")

        return {'FINISHED'}


class StopCaptureOperator(bpy.types.Operator):
    bl_idname = "livepose.livepose_stop"
    bl_label = "Stop capture"
    bl_description = "Stop retrieving LivePose data"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        # print("Keypoints local dict :\n", kp_local_dict)

        threads = [t for t in threading.enumerate() if t.is_alive()]

        if len(threads) > 1:
            for i in range(1, len(threads)):
                threads[i].stop()
                print(threads[i].name)
        else:
            self.report({'INFO'}, "No capture to stop.")
        # print("Inverted posebone's dict :\n", inv_pb_dict)
        return {'FINISHED'}


# ---------- Pick armature to control ----------#

class PickArmatureOperator(bpy.types.Operator):
    bl_idname = "livepose.livepose_armature"
    bl_label = "Apply rig"
    bl_description = "Pick armature to control"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):

        armature_obj = objects.get_obj(context.scene.Armature)
        avatar_obj = objects.get_obj(context.scene.Avatar)
        return armature_obj is not None and avatar_obj is not None

    def execute(self, context):
        global kp_local_dict

        # Activate Rigify if not enabled
        is_enabled, is_loaded = check("rigify")
        if not is_enabled:
            bpy.ops.preferences.addon_enable(module="rigify")

        # Get selected armature object
        armature_name = context.scene.Armature
        armature_obj = objects.get_obj(armature_name)
        if armature_obj is None:
            self.report({'ERROR'}, armature_name + " not found in the scene.")
            return {'CANCELLED'}

        self.rigifyIK(armature_obj)

        return {'FINISHED'}

    def rigifyIK(self, obj):
        global inv_pb_dict, inv_world

        # Get avatar mesh object
        context = bpy.context
        avatar_name = context.scene.Avatar
        bpy.context.view_layer.objects.active = bpy.data.objects[avatar_name]
        # Get into object mode
        bpy.ops.object.mode_set(mode='OBJECT')
        # Apply scale
        bpy.ops.object.transform_apply(
            location=False, rotation=False, scale=True)

        # Generate Rigify rig
        rigify.generate.generate_rig(context, obj)
        rig = bpy.data.objects[context.object.name]
        rig_pose = rig.pose   # rig for editing bones in pose mode

        # Store rig initial bones position in a dict
        initial_dict = objects.initial_position(rig_pose)
        # Store all initial bones inverse matrix
        inv_pb_dict = objects.get_inv_pb_dict(rig_pose)
        # Store world inverse matrix
        inv_world = rig.matrix_world.inverted()

        # Initialize LivePose keypoints dict with rig dict
        kp_local_dict = livePoseWrapper.initialize_with(initial_dict)

        # Set parenting
        avatar_obj = objects.get_obj(avatar_name)
        # Get avatar dimensions for the calibration panel
        context.scene.CapturePropertyGroup.model_dim[0] = avatar_obj.dimensions.x
        context.scene.CapturePropertyGroup.model_dim[1] = avatar_obj.dimensions.y

        if avatar_obj != None:
            self.parent_to_armature(rig, avatar_obj)

            # Apply limit distance constraints
            # livePoseWrapper.set_limit_distance_constraints(rig_pose)

        else:
            self.report({'ERROR'}, avatar_obj + " not found in the scene.")
            return {'CANCELLED'}

    def parent_to_armature(self, rig, avatar):

        # Get into object mode
        bpy.ops.object.mode_set(mode='OBJECT')
        # Select rig
        rig.select_set(True)

        # Go into pose mode
        bpy.ops.object.mode_set(mode='POSE')
        # Select all bones
        bpy.ops.pose.select_all(action='SELECT')
        bpy.data.armatures[rig.name].layers[29] = True

        # Go back into object mode
        bpy.ops.object.mode_set(mode='OBJECT')
        # Parent the mesh to the armature rig
        avatar.select_set(True)  # Must select the avatar mesh first
        rig.select_set(True)  # Then select the armature rig
        bpy.ops.object.parent_set(type='ARMATURE_AUTO')  # then parent

        # Go back into pose mode
        bpy.ops.object.mode_set(mode='POSE')
        bpy.data.armatures[rig.name].layers[29] = False


class ExportBVH(bpy.types.Operator):
    """Export to BVH format"""

    bl_idname = "lp.export_bvh"
    bl_label = "export_bvh"
    bl_options = {'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.active_object:
            if context.active_object.type == "ARMATURE":
                return True

    def execute(self, context):
        try:
            bpy.ops.export_anim.bvh('INVOKE_DEFAULT')
        finally:
            pass

        return {'FINISHED'}


class ExportFBX(bpy.types.Operator):
    """Export to FBX format"""

    bl_idname = "lp.export_fbx"
    bl_label = "export_fbx"
    bl_options = {'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.active_object:
            if context.active_object.type == "ARMATURE":
                return True

    def execute(self, context):
        try:
            bpy.ops.export_scene.fbx('INVOKE_DEFAULT')
        finally:
            pass

        return {'FINISHED'}


class ExportGLTF(bpy.types.Operator):
    """Export to GLTF format"""

    bl_idname = "lp.export_gltf"
    bl_label = "export_gltf"
    bl_options = {'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.active_object:
            if context.active_object.type == "ARMATURE":
                return True

    def execute(self, context):
        try:
            bpy.ops.export_scene.gltf('INVOKE_DEFAULT')
        finally:
            pass

        return {'FINISHED'}
