import bpy
from bpy.props import PointerProperty
from bpy.utils import register_class, unregister_class

from . import ui_panels, ui_dependencies
from ..utils import install_dependencies

# ---------- Registering ---------------#


def get_classes():
    # Get classes to avoid loading possibly unavailable packages
    classes = (
        ui_panels.LivePosePanel, ui_panels.CapturePropertyGroup, ui_panels.LivePoseResetCalibration,
        ui_panels.LivePoseSaveUserPrefs, ui_panels.StartCaptureOperator, ui_panels.StopCaptureOperator,
        ui_panels.PickArmatureOperator, ui_panels.ExportBVH, ui_panels.ExportFBX, ui_panels.ExportGLTF
    )
    return classes


def get_preferences():
    preference_classes = (ui_dependencies.InstallDependenciesButton,
                          ui_dependencies.LivePosePreferences,
                          ui_panels.WarningPanel)
    return preference_classes


def register():
    # Register LivePose Addon
    for cls in get_preferences():
        register_class(cls)

    try:
        print('Try to access dependencies')
        for dependency in install_dependencies.dependencies:
            print(str(dependency))
            install_dependencies.import_module(
                module_name=dependency.module, global_name=dependency.name)
        install_dependencies.dependencies_installed = True
        # Register interface
        register_user_interface()

    except ModuleNotFoundError:
        # Don't register other panels, etc.
        print('REGISTRATION FAILED: REQUIRED MODULE NOT FOUND')
        return


def register_user_interface():
    # Register LivePose Addon interface
    for cls in get_classes():
        register_class(cls)
    bpy.types.Scene.CapturePropertyGroup = PointerProperty(
        type=ui_panels.CapturePropertyGroup)


def unregister():
    # Unregister LivePose Addon
    for cls in get_preferences():
        unregister_class(cls)

    if install_dependencies.dependencies_installed:
        # Unregister LivePose Addon with active dependencies
        classes = get_classes()
        for cls in reversed(classes):
            unregister_class(cls)

        del bpy.types.Scene.CapturePropertyGroup


if __name__ == "__main__":
    register()
