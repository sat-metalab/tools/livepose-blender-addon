# I have followed the method @cgtinker used for BlendArMocap to create an inbuilt dependencies install button in
# Blender, in its ui_preferences.py script: https://github.com/cgtinker/BlendArMocap/blob/main/src/cgt_blender
# /interface/ui_preferences.py

import subprocess
import os

import bpy

from . import ui_registration
from ..utils import install_dependencies


class InstallDependenciesButton(bpy.types.Operator):
    bl_idname = "button.lp_install_dependencies"
    bl_label = "Install dependencies"
    bl_description = ("Downloads and installs the required python packages for this add-on. "
                      "Internet connection is required. Blender may have to be started with "
                      "elevated permissions in order to install the package")
    bl_options = {"REGISTER", "INTERNAL"}

    @classmethod
    def poll(cls, context):
        # Deactivate install button when dependencies have been installed
        return not install_dependencies.dependencies_installed

    def execute(self, context):
        try:
            # try to install dependencies
            install_dependencies.install_pip()
            install_dependencies.update_pip()
            for dependency in install_dependencies.dependencies:
                install_dependencies.install_and_import_module(module_name=dependency.module,
                                                               package_name=dependency.package,
                                                               global_name=dependency.name)
        except (subprocess.CalledProcessError, ImportError) as err:
            self.report({"ERROR"}, str(err))
            return {"CANCELLED"}

        # register user interface after installing dependencies
        install_dependencies.dependencies_installed = True
        ui_registration.register_user_interface()

        return {"FINISHED"}


class LivePosePreferences(bpy.types.AddonPreferences):
    bl_idname = os.path.basename(os.path.dirname(os.path.dirname(__file__)))

    def draw(self, context):
        layout = self.layout
        layout.operator(InstallDependenciesButton.bl_idname, icon="CONSOLE")
