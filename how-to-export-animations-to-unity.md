# Setting up exported animations to work into Unity

> [French version](#french-version) below.

### English version

---

After recording an animation using the add-on, you've just exported your `.fbx` animation file from Blender.

Now import this file into a Unity scene. Here, the `dummy_test.fbx` file for instance.

<div align="center">
  <figure>
  <img src="./media/images/how-to-export-animations/fig1.png" />  
  <figcaption><b>- fig.1: imported asset -</b></figcaption>
  </figure>
</div>

Then you must:

- “drag & drop” the corresponding asset (_fig.1_) in the scene.  
  <br>

<div align="center">

  <figure>
  <img src="./media/images/how-to-export-animations/fig2.png" />  
  <figcaption><b>- fig.2: animation controller icon -</b></figcaption>
  </figure>
  
  <figure>
  <img src="./media/images/how-to-export-animations/fig3.png" width="600" height="400"/> 
  <figcaption><b>- fig.3: animation menu overview -</b></figcaption>
  </figure>
</div>

- create an animation controller (_Right Click_ into _Assets > Create > Animator Controller_) (_fig.2_)
  - (optionally, rename the new controller (to `Animator` for example))
  - double-click on the controller to open the animation menu (_fig.3_)
  - “drag & drop” the animation of the asset (_fig.1_) in this menu
  - the transition is normally created automatically
    <br>

<div align="center">

  <figure>
  <img src="./media/images/how-to-export-animations/fig4.png" width="400" height="500" />
  <figcaption><b>- fig.4: animator component -</b></figcaption>
  </figure>

</div>

- associate the animation controller created with the model in the scene

  - choose the model in the scene
  - _Add Component > Animator_ then select `Controller: Animator` (_fig.4_)
    <br>

- the animation will start when the scene is launched.
  <br>

### French version

---

Après avoir enregistré une animation à l'aide de l'add-on, vous venez d'exporter votre fichier d'animation `.fbx` depuis Blender.

Maintenant, importez ce fichier dans une scène Unity. Ici, c'est le fichier `dummy_test.fbx` qui est utilisé pour l'exemple.

Il vous faut alors ensuite :

- “drag & drop” l’asset (_fig.1_) correspondant dans la scène.

- créer un contrôleur d’animation (_Clic droit_ puis _Assets > Create > Animator Controller_) (_fig.2_)
  - (optionnellement, renommer le nouveau contrôleur (en `Animator` par exemple))
  - double-cliquer sur le contrôleur pour ouvrir le menu d’animation (_fig.3_)
  - “drag & drop” l’animation de l’asset (_fig.1_) dans ce menu
  - la transition se créée normalement automatiquement
- associer le contrôleur d’animation créé au modèle dans la scène
  - choisir le modèle dans la scène
  - _Add Component > Animator_ puis sélectionner `Controller: Animator` (_fig.4_)
- l’animation se lancera au lancement de la scène.
